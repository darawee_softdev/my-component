
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 66868
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product (1,"Choco 1",40,"1.jpg"));
        list.add(new Product (2,"Choco 2",40,"1.jpg"));
        list.add(new Product (3,"Choco 2",40,"1.jpg"));
        list.add(new Product (4,"Milk 1",35,"2.jpg"));
        list.add(new Product (5,"Milk  2",35,"2.jpg"));
        list.add(new Product (6,"Milk  3",35,"2.jpg"));
        list.add(new Product (7,"Soda 1",25,"3.jpeg"));
        list.add(new Product (8,"Soda 2",25,"3.jpeg"));
        list.add(new Product (9,"Soda 3",25,"3.jpeg"));
        list.add(new Product (10,"Soda 4",25,"3.jpeg"));
        return list;
}
}
